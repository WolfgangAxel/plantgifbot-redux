#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

########################################################################
#   PLANT GIF BOT                                                      #
########################################################################
# 
# Archiving Freyja's growth through hourly screenshots! Updating on
# Imgur every day, week, and month, automatically!
# 

from praw import Reddit
from imgurpython import ImgurClient
from PIL import Image, ImageDraw
from postgres import Postgres
from requests import get
from io import BytesIO
from os import mkdir, remove, listdir
from os.path import basename, dirname, exists, join
from re import search
from time import time, sleep, strftime, gmtime, mktime
from sys import argv
from math import cos, sin, pi

import secrets

if "--help" in argv or "-h" in argv:
    exit("Run using cron. Usage:"+
        "\n --dry no uploads"+
        "\n --force force gif creation"+
        "\n --debug lotsa output")

DEBUG = "--debug" in argv
FORCE = "--force" in argv
DRY = "--dry" in argv


DAY_AL = "rGyhH"
WEK_AL = "3bCHO"
MTH_AL = "aKTcv"

snap_url = ("https://nexusapi-us1.dropcam.com/get_image"+
                 "?uuid=2b5dc4d1a6974df9a44f0bb9bad13216&width=560")
subreddit = "takecareofmyplant"

yes_inner_regex = ['yes', 'aye', 'prost', 'ja']
no_inner_regex = ['no', 'not on your nelly', 'nein?']

BASEDIR = dirname(__file__)

def newOrd(n):
    # Thanks, /u/TylerJayWood
    if 4<=n%100<=20:
        return str(n)+"th"
    else:
        return str(n)+{1:"st",2:"nd",3:"rd"}.get(n%10,"th")

def get_req(func, *args, **kwargs):
    """
    Get something. Try 5 times, then wait one minute for 10 minutes
    """
    overall = 0
    if DEBUG:
        print("get_req:",func.__name__,args,kwargs)
    while overall < 10:
        overall += 1
        attempt = 0
        while attempt < 5:
            try:
                if DEBUG:
                    out = func(*args, **kwargs)
                    print("get_req returning", func.__name__, out)
                    return out
                return func(*args, **kwargs)
            except Exception as E:
                attempt += 1
                if DEBUG:
                    print("get_req ERROR:",E,
                          "\nget_req:",overall,attempt)
            sleep(2)
        sleep(50)
    if DEBUG:
        print("get_req FAILED")
    return None

def get_votes(post):
    """
    Get the votes in a watering thread from the db
    Input a praw `subreddit`
    """
    ys = len(get_req(P.all,"SELECT * FROM thread_votes WHERE"+
                " thread_id='{}' AND comment_vote=1".format(post.id)))
    ns = len(get_req(P.all,"SELECT * FROM thread_votes WHERE"+
                " thread_id='{}' AND comment_vote=-1".format(post.id)))
    return ys, ns

def get_votes_manual(post):
    """
    Get the votes in a watering thread manually
    (backup if db inaccessable)
    """
    # Tally yes/no votes
    ys = 0
    ns = 0
    get_req(post.comments.replace_more,limit=None)
    for comment in get_req(post.comments.list):
        if comment.author == "takecareofmyplant":
            # Skip the bot
            continue
        # Count number of failed regexes, subtract from total regexes
        yes = bool( len(yes_inner_regex) - (
              [search(r'(?i)\b{}\b'.format(r), comment.body) 
                for r in yes_inner_regex].count(None))
              )
        no = bool( len(no_inner_regex) - (
              [search(r'(?i)\b{}\b'.format(r), comment.body) 
                for r in no_inner_regex].count(None))
              )
        if DEBUG:
            print("get_votes:",comment.body, yes, no)
        if yes == no:
            # No vote
            continue
        ys += int(yes) # int(True) = 1
        ns += int(no)  # int(False) = 0
    if DEBUG:
        print("get_votes success!", ys, ns)
    return ys, ns

def textbox(d, text, x, y, align="left"):
    """
    Make a black box behind some white text
    Pass only 1 line of text
    """
    text = text.strip()
    size = len(text)
    width = size * 6 + 1
    offset = {
        "center":int(width/2), 
        "right":width
        }.get(align,0)
    x_1 = x-3 - offset
    y_1 = y
    x_2 = x + width - offset
    y_2 = y+10
    #d.rectangle((x_1, y_1, x_2, y_2),(0,0,0))
    d.text((x-offset, y), text)

def draw(frame, yes, no, now):
    """
    Handle drawing on the image
    """
    d = ImageDraw.Draw(frame)
    # Add a black box
    d.rectangle((328,0,400,244),(0,0,0))
    d.text((331,0),strftime("%Y-%m-%d\n%H:%M (%a)",
                            gmtime())+"\nYES     NO")
    d.rectangle((331,27,397,28),(255,255,255))
    
    # Colors
    yes_color = (50,100,255)
    no_color = (255,50,100)
    moist_color = (100, 100, 255)
    temp_color = (255, 100, 50)
    humid_color = (50, 150, 150)
    
    # Vote bar
    total = yes + no + 1 # help lower flashing of colors at low values
    midp = 364 + round(33*(yes-no)/total)
    d.rectangle((331,41,midp,56), yes_color)
    d.rectangle((midp,41,397,56), no_color)
    textbox(d, str(yes), 338, 44)
    textbox(d, str(no), 394, 44, "right")
    
    
    # Sensor bars
    data = get_req(P.one,
        "SELECT * FROM sensor_json ORDER BY time_stamp DESC LIMIT 1")
    if mktime(now) - mktime(data.time_stamp.timetuple()) >= 6*60*60:
        # Make everything 0 if the data is older than 6 hours
        value = {key: 1024 for key in ('top_soil','mid_soil','tray')}
        for key in data.values_json:
            data.values_json[key] = value.get(key, 0)
    
    # Moisture
    top = (1024-data.values_json['top_soil'])/1024
    mid = (1024-data.values_json['mid_soil'])/1024
    btm = (1024-data.values_json['tray'])/1024
    h = 58
    d.text((331,h)," Moisture")
    d.rectangle((331,h+12,397,h+27),(128,128,128))
    d.rectangle((331,h+29,397,h+44),(128,128,128))
    d.rectangle((331,h+46,397,h+61),(128,128,128))
    d.rectangle((331,h+12,int(331+66*top),h+27),moist_color)
    d.rectangle((331,h+29,int(331+66*mid),h+44),moist_color)
    d.rectangle((331,h+46,int(331+66*btm),h+61),moist_color)
    textbox(d, "t:"+str(top)[:5], 
                364, h+15, "center")
    textbox(d, "m:"+str(mid)[:5], 
                364, h+32, "center")
    textbox(d, "b:"+str(btm)[:5], 
                364, h+49, "center")

    # Sunlight
    r = 9
    cx = 364
    cy = 140
    shade = int(50 + min(data.values_json['sunlight'],205))
    sun_color = (shade,shade,50)
    d.ellipse((cx-r,cy-r,cx+r,cy+r),sun_color)
    # Draw triangles
    for i in range(7):
        rad = 2*pi/7*i
        points = (
            (round(cx+17*sin(rad)),round(cy-17*cos(rad))),
            (round(cx+11*sin(rad-pi/20)),round(cy-11*cos(rad-pi/20))),
            (round(cx+11*sin(rad+pi/20)),round(cy-11*cos(rad+pi/20))),
        )
        d.polygon(points,sun_color)

    # Temperature
    temp = sorted(
        ("<60",     # This is so janky but I love it.
        " "+str(data.values_json["temperature_F"]),
        ">80"),
        key = lambda x: x[1:])[1]
    temp_size = (eval(temp[1:])-60)/20*66
    d.text((331, 158),"  °F Temp")
    d.rectangle((331,171,397,186),(128,128,128))
    d.rectangle((331,171,int(331+temp_size),186),temp_color)
    textbox(d,temp,364,174,"center")
    
    # Humidity
    humid = sorted(
        ("<25",     # This is so janky but I love it.
        " "+str(data.values_json["humidity"]),
        ">60"),
        key = lambda x: x[1:])[1]
    humid_size = (eval(humid[1:])-25)/35*66
    d.text((331, 189)," % Humidity")
    d.rectangle((331,202,397,217),(128,128,128))
    d.rectangle((331,202,int(331+humid_size),217),humid_color)
    textbox(d,humid,364,205,"center")

    if DEBUG:
        frame.show()
        print("draw success!")
    

def make_frame():
    """
    Master function for making frames
    """
    # Make a wrapper function to validate the image data before
    # we return it since get doesn't raise errors on 404's
    def get_img_validated(snap_url):
        img = get(snap_url)
        assert img.status_code == 200
        return img
    img_data = get_req(get_img_validated,snap_url)
    if not img_data:
        if DEBUG:
            print("make_frame ABORTED: No image")
        return None
    frame = Image.open(BytesIO(img_data.content))
    if frame.width != 400:
        frame = frame.resize((400,224),Image.ANTIALIAS)
    try:
        post = get_req(R.subreddit(subreddit).sticky,number=2)
    except:
        # No sticky up right now, no voting going on
        post = None
    yes, no = get_votes(post) if post else (0,0)
    now = gmtime()
    draw(frame, yes, no, now)
    frame.save(join(BASEDIR,"daily","snaps",strftime(
                "%y-%m-%d-%H-%M",now)+".png"))
    if DEBUG:
        print("make_frame success!")

def animate(fps, outpath):
    """
    Takes a string of image filepaths, turns it into a gif
    Cleans up and copies data as well
    """
    frames = []
    # We assume they have been sorted
    for fp in fps:
        if DEBUG:
            print("animate:", fp)
        frame = Image.open(fp)
        # Convert the frame to an optimized palette to save space
        #frame = frame.convert("P", palette=Image.ADAPTIVE, colors=64)
        frames.append(frame)
    frames = [frames[0]] + frames
    frames[0].save(outpath, save_all = True, append_images = frames,
                   #  make the gif 10 seconds, always
                   duration = (10000/(len(frames)+1)), loop = 0)
    if DEBUG:
        print("animate success!")

def gif_worker(R, I, now, freq, album, description, title):
    """
    Make a plant gif!
    
    Assemble all snaps into a gif, save it, and upload it
    """
    if DEBUG:
        print("gif_worker:", R, I, now, freq, album, description, title)
    # Make the gif and save it
    freq_dir = join(BASEDIR,freq)
    fps = sorted( (join(freq_dir,"snaps",fp) for fp in 
                      listdir(join(freq_dir,"snaps"))) )
    if len(fps) <= 1:
        return []
    gif = join(freq_dir, strftime("%y-%m-%d",now)+".gif")
    animate(fps,gif)
    # Upload it to Imgur
    config = {
        "album":album,
        "title":title,
        "description":description}
    if DRY:
        print(gif, config)
        uploaded = True
    else:
        uploaded = get_req(I.upload_from_path,gif,
                           config=config,anon=False)
    # Post it to Reddit
    if uploaded and DRY:
        print(description+
                "[Click here for more "+freq+" plant gifs](https://www"+
                ".reddit.com/r/"+subreddit+"/search?q=%5B"+
                freq[0].upper()+"PG%5D&restrict_sr=on&sort=relevance&t"+
                "=all) or [click here to see them all at once](http://"+
                "imgur.com/a/"+album+")")
    elif uploaded:
        rPost = get_req(R.subreddit(subreddit).submit,
                        title, url=uploaded['link']+"v")
        get_req(rPost.disable_inbox_replies)
        get_req(rPost.reply,description+
                "[Click here for more "+freq+" plant gifs](https://www"+
                ".reddit.com/r/"+subreddit+"/search?q=%5B"+
                freq[0].upper()+"PG%5D&restrict_sr=on&sort=relevance&t"+
                "=all) or [click here to see them all at once](http://"+
                "imgur.com/a/"+album+")")
    if DEBUG:
        print("gif_worker success!")
    return fps

def daily_gif(R, I, now):
    """
    Makes a daily gif!
    """
    if DEBUG:
        print("daily_gif", R, I, now)
    # Get watering result from previous day
    db_post = get_req(P.all,"SELECT * FROM water_thread_id"+
                    " ORDER BY time_stamp DESC LIMIT 2")
    for listing in db_post:
        post = get_req(R.submission,id=listing.post_id)
        # get the post that's more than 12 hours old but less than 36
        delta = time() - post.created_utc
        if 12*60*60 < delta < 36*60*60:
            break
        post = None
    if not post:
        # Revert to legacy
        for post in get_req(
            R.redditor("takecareofmyplant").submissions.new, limit=5):
            # Make sure it was yesterday's post by adding a day to 
            # the created date and comparing to today's day
            # Done this way to handle year changes.
            yd = gmtime(post.created_utc+24*60*60).tm_yday
            if "Today is" in post.title and yd == now.tm_yday:
                break
        # Abort
        if DEBUG:
            print("daily_gif ABORTED: No post found")
        return None
    yes, no = get_votes(post)
    if yes > no:
        description = "Freyja was watered during this gif."
    else:
        description = "Freyja was not watered during this gif."
    fancyDay = newOrd(int(strftime("%-d",now)))
    fps = gif_worker(R, I, now, "daily", DAY_AL, description+"\n\n",
                "[DPG] Daily Plant Gif for "+
                strftime("%A, %B ",now)+fancyDay)
    # Clean up snap folder and move some images elsewhere
    for i,fp in enumerate(fps):
        if DEBUG:
            print("daily_gif cleanup:", i, fp)
        # move the most recent one to monthly
        if i == len(fp):
            if DEBUG:
                print("Moving to monthly")
            with open(fp,'rb') as orig:
                with open(join(BASEDIR,"monthly","snaps", basename(fp)),
                            'wb') as new:
                    new.write(orig.read())
        # Move 1 out of every 7 to weekly
        if i % 7 == 0:
            if DEBUG:
                print("Moving to weekly")
            with open(fp,'rb') as orig:
                with open(join(BASEDIR,"weekly","snaps", basename(fp)),
                            'wb') as new:
                    new.write(orig.read())
        remove(fp)

def weekly_gif(R, I, now):
    """
    Makes a weekly gif!
    """
    if DEBUG:
        print("weekly_gif", R, I, now)
    description = ""
    fps = gif_worker(R, I, now, "weekly", WEK_AL, description,
                "[WPG] Weekly Plant Gif for "+
                strftime("%b %d ", gmtime(time()-7*24*60*60))+
                "through"+
                strftime("%b %d %Y"))
    for i,fp in enumerate(fps):
        remove(fp)

def monthly_gif(R,I,now):
    """
    Makes a monthly gif!
    """
    if DEBUG:
        print("monthly_gif", R, I, now)
    description = ""
    fps = gif_worker(R, I, now, "monthly", MTH_AL, description,
                "[MPG] Monthly Plant Gif for "+
                strftime("%B %Y", gmtime(time()-7*24*60*60)))
    for i,fp in enumerate(fps):
        remove(fp)

if __name__ == "__main__":
    # Filesystem orientation
    for folder in ("daily", "weekly", "monthly"):
        parent = join(BASEDIR,folder)
        if not exists(parent):
            mkdir(parent)
        if not exists(join(parent,"snaps")):
            mkdir(join(parent,"snaps"))
    R = Reddit(
        client_id = secrets.R_CLID,
        client_secret = secrets.R_SCRT,
        password = secrets.R_PASS,
        user_agent = ("Making gifs of Freyja the plant for /r/"+
                        subreddit+" by /u/"+secrets.MY_REDDIT_USER),
        username = secrets.R_USER
    )
    I = ImgurClient(
        secrets.I_CLID,
        secrets.I_SCRT,
        secrets.I_AXSS,
        secrets.I_REFR
    )
    P = get_req(Postgres,secrets.POSTGRES_URL)
    
    now = gmtime()
    make_frame()
    
    if (now.tm_hour == 15 and now.tm_min < 15) or FORCE:
        daily_gif(R, I, now)
        if now.tm_wday == 0:
            weekly_gif(R, I, now)
        if now.tm_mday == 1:
            monthly_gif(R, I, now)
