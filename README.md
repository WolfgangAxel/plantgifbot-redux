# PlantGifBot-Redux

Rework of [PlantGifBot](https://gitlab.com/WolfgangAxel/PlantGifBot). Will be wonky for a bit while things get set up/working again on both TJW's and my side.

It always bothered me how spaghetti-coded the original PGB was, so I decided to rewrite it for the new plant. I'm not sure if it's any *less* spaghetti-coded now, but I like to think it's an improvement, if nothing else.

## Long Live Freyja!
